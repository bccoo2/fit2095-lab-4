const express = require('express');
const router = express.Router();

let db = [];

router.get('/', function(req, res) {
    res.render('index', {pageTitle: 'Index'})
});

router.get('/newTask', function (req,res) {
    res.render('newTask', {pageTitle: 'New Task'})
});

router.get('/listTasks', function(req, res){
    res.render('listTasks', {pageTitle: 'Task List', tasks : db});
});

router.post('/addtask', function(req, res){
    let data = {
        name: req.body.name,
        due: req.body.due,
        desc: req.body.desc
    };
    db.push(data);
    res.render('listTasks', {pageTitle: 'New Task', tasks: db});
});

module.exports = router;

